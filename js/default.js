$(document).ready(function() {
	$('.element').atrotating({
		type: 'text',
		method: 'group',

		animation: 'fade',
		animationSpeed: 300,
		animationEasing: 'swing',
		animationType: 'full',

		delay: 1000,
		delayGroup: 2000,
		delayStart: 0,

		animateOne: false,
		separator: '|',
		reverse: false,
		trim: false
	});
});